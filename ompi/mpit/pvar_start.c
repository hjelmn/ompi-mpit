/* -*- Mode: C; c-basic-offset:4 ; indent-tabs-mode:nil -*- */
/*
 * Copyright (c) 2012      Los Alamos National Security, LLC. All rights
 *                         reserved.
 * $COPYRIGHT$
 *
 * Additional copyrights may follow
 *
 * $HEADER$
 */

#include "ompi/include/ompi_config.h"
#include "ompi/mpit/mpit-internal.h"

#include "opal/mca/base/mca_base_param.h"

#include "ompi/runtime/params.h"
#include "ompi/communicator/communicator.h"
#include "ompi/mpi/c/bindings.h"
#include "ompi/errhandler/errhandler.h"
#include "ompi/constants.h"

static const char FUNC_NAME[] = "MPI_T_pvar_start";

int MPI_T_pvar_start(MPI_T_pvar_session session, MPI_T_pvar_handle handle)
{
    mpit_lock ();

    if (!mpit_is_initialized ()) {
        mpit_unlock ();
        return MPI_T_ERR_NOT_INITIALIZED;
    }

    mpit_unlock ();

    /* XXX -- TODO -- Implement me */
    return MPI_T_ERR_INVALID_HANDLE;
}
