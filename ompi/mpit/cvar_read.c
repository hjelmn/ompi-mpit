/* -*- Mode: C; c-basic-offset:4 ; indent-tabs-mode:nil -*- */
/*
 * Copyright (c) 2012      Los Alamos National Security, LLC. All rights
 *                         reserved.
 * $COPYRIGHT$
 *
 * Additional copyrights may follow
 *
 * $HEADER$
 */

#include "ompi/include/ompi_config.h"

#include "opal/mca/base/mca_base_param.h"

#include "ompi/runtime/params.h"
#include "ompi/mpi/c/bindings.h"
#include "ompi/errhandler/errhandler.h"
#include "ompi/constants.h"

#include "ompi/mpit/mpit-internal.h"

static const char FUNC_NAME[] = "MPI_T_cvar_read";

int MPI_T_cvar_read (MPI_T_cvar_handle handle, void *buf)
{
    char *tmp = NULL;
    int rc;

    mpit_lock ();

    if (0 == mpit_init_count) {
        return MPI_T_ERR_NOT_INITIALIZED;
    }

    switch (handle->info->mbpp_type) {
    case MCA_BASE_PARAM_TYPE_STRING:
        rc = mca_base_param_lookup_string (handle->info->mbpp_index, &tmp);
        if (OPAL_SUCCESS == rc && NULL != tmp) {
            strcpy ((char *) buf, tmp);
            free (tmp);
        } else {
            ((char *)buf)[0] = '\0';
        }

        break;
    case MCA_BASE_PARAM_TYPE_INT:
        rc = mca_base_param_lookup_int (handle->info->mbpp_index, (int *) buf);
        
        break;
    default:
        return MPI_ERR_OTHER;
    }

    mpit_unlock ();

    return rc;
}
