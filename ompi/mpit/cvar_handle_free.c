/* -*- Mode: C; c-basic-offset:4 ; indent-tabs-mode:nil -*- */
/*
 * Copyright (c) 2012      Los Alamos National Security, LLC. All rights
 *                         reserved.
 * $COPYRIGHT$
 *
 * Additional copyrights may follow
 *
 * $HEADER$
 */

#include "ompi/include/ompi_config.h"

#include "opal/mca/base/mca_base_param.h"

#include "ompi/runtime/params.h"
#include "ompi/mpi/c/bindings.h"
#include "ompi/errhandler/errhandler.h"
#include "ompi/constants.h"

#include "ompi/mpit/mpit-internal.h"

int MPI_T_cvar_handle_free (MPI_T_cvar_handle *handle)
{
    mpit_lock ();

    if (0 == mpit_init_count) {
        mpit_unlock ();
        return MPI_T_ERR_NOT_INITIALIZED;
    }

    OBJ_RELEASE((*handle)->info);

    free (*handle);
    *handle = NULL;

    mpit_unlock ();

    return MPI_SUCCESS;
}
