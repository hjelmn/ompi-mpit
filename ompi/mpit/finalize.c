/* -*- Mode: C; c-basic-offset:4 ; indent-tabs-mode:nil -*- */
/*
 * Copyright (c) 2012      Los Alamos National Security, LLC. All rights
 *                         reserved.
 * $COPYRIGHT$
 *
 * Additional copyrights may follow
 *
 * $HEADER$
 */


#include "ompi/include/ompi_config.h"
#include "ompi/mpit/mpit-internal.h"

#include "opal/mca/base/mca_base_param.h"

#include "ompi/runtime/params.h"
#include "ompi/runtime/mpiruntime.h"
#include "ompi/communicator/communicator.h"
#include "ompi/mpi/c/bindings.h"
#include "ompi/errhandler/errhandler.h"
#include "ompi/constants.h"

#include "opal/include/opal/sys/atomic.h"

static const char FUNC_NAME[] = "MPI_T_finalize";

int MPI_T_finalize (void)
{
    int rc;

    mpit_lock ();

    if (0 == mpit_init_count) {
        mpit_unlock ();
        return MPI_T_ERR_NOT_INITIALIZED;
    }

    mpit_init_count--;

    if (0 == mpit_init_count) {
        rc = ompi_mpi_finalize ();
    }

    mpit_unlock ();

    return rc;
}
