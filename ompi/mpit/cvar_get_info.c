/* -*- Mode: C; c-basic-offset:4 ; indent-tabs-mode:nil -*- */
/*
 * Copyright (c) 2012      Los Alamos National Security, LLC. All rights
 *                         reserved.
 * $COPYRIGHT$
 *
 * Additional copyrights may follow
 *
 * $HEADER$
 */

#include "ompi/include/ompi_config.h"

#include "opal/mca/base/mca_base_param.h"

#include "ompi/runtime/params.h"
#include "ompi/communicator/communicator.h"
#include "ompi/mpi/c/bindings.h"
#include "ompi/errhandler/errhandler.h"
#include "ompi/constants.h"

#include "ompi/mpit/mpit-internal.h"

static const char FUNC_NAME[] = "MPI_T_cvar_get_info";

int MPI_T_cvar_get_info(int cvar_index, char *name, int *name_len, int *verbosity,
			MPI_Datatype *datatype, MPI_T_enum *enumtype, char *desc,
			int *desc_len, int *bind, int *scope)
{
    mca_base_param_info_t *info;
    int rc;

    mpit_lock ();

    if (0 == mpit_init_count) {
        mpit_unlock ();
        return MPI_T_ERR_NOT_INITIALIZED;
    }

    rc = mca_base_param_dump_index (cvar_index, &info);
    if (OPAL_SUCCESS != rc) {
        mpit_unlock ();

	switch (rc) {
	case OPAL_ERR_VALUE_OUT_OF_BOUNDS:
	    return MPI_T_ERR_INVALID_INDEX;
	}

        return MPI_ERR_OTHER;
    }

    mpit_copy_string (name, name_len, info->mbpp_full_name);
    mpit_copy_string (desc, desc_len, info->mbpp_help_msg);

    switch (info->mbpp_type) {
    case MCA_BASE_PARAM_TYPE_INT:
	*datatype = MPI_INT;
	break;
    case  MCA_BASE_PARAM_TYPE_STRING:
	*datatype = MPI_CHAR;
	break;
    default:
        /* Internal error! Did the MCA parameter system change? */
        assert (0);
        mpit_unlock ();
	return MPI_ERR_OTHER;
    }

    if (NULL != enumtype) {
	*enumtype = MPI_T_ENUM_NULL;
    }

    /* XXX -- TODO -- Make some (or all) MCA variabes writable (eventually) */
    if (NULL != scope) {
        *scope = MPI_T_SCOPE_READONLY;
    }

    /* XXX -- TODO -- Update MCA to support object binding */
    if (NULL != bind) {
        *bind = MPI_T_BIND_NO_OBJECT;
    }

    OBJ_RELEASE(info);

    mpit_unlock ();

    return MPI_SUCCESS;
}
