/* -*- Mode: C; c-basic-offset:4 ; indent-tabs-mode:nil -*- */
/*
 * Copyright (c) 2012      Los Alamos National Security, LLC. All rights
 *                         reserved.
 * $COPYRIGHT$
 *
 * Additional copyrights may follow
 *
 * $HEADER$
 */


#include "ompi/include/ompi_config.h"

#include <sched.h>

#include "ompi/mpit/mpit-internal.h"

#include "opal/mca/base/mca_base_param.h"

#include "ompi/runtime/params.h"
#include "ompi/runtime/mpiruntime.h"
#include "ompi/communicator/communicator.h"
#include "ompi/mpi/c/bindings.h"
#include "ompi/errhandler/errhandler.h"
#include "ompi/constants.h"

#include "opal/include/opal/sys/atomic.h"

static const char FUNC_NAME[] = "MPI_T_init_thread";

opal_mutex_t mpit_big_lock = OPAL_MUTEX_STATIC_INIT;

volatile uint32_t mpit_init_count = 0;

void mpit_lock (void)
{
    opal_mutex_lock (&mpit_big_lock);
}

void mpit_unlock (void)
{
    opal_mutex_unlock (&mpit_big_lock);
}

int MPI_T_init_thread (int required, int *provided)
{
    int rc = MPI_SUCCESS;

    mpit_lock ();

    do {
        if (0 != mpit_init_count++) {
            break;
        }

        /* XXX -- TODO -- break ompi_init init into two functions. one that
           initializes MCA and the datatypes and another that does the rest */

        rc = ompi_mpi_init (0, NULL, required, provided);
    } while (0);

    mpit_unlock ();

    return rc;
}
