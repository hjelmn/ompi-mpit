/* -*- Mode: C; c-basic-offset:4 ; indent-tabs-mode:nil -*- */
/*
 * Copyright (c) 2012      Los Alamos National Security, LLC. All rights
 *                         reserved.
 * $COPYRIGHT$
 *
 * Additional copyrights may follow
 *
 * $HEADER$
 */

#include "ompi/include/ompi_config.h"
#include "ompi/mpit/mpit-internal.h"

#include "opal/mca/base/mca_base_param.h"

#include "ompi/runtime/params.h"
#include "ompi/communicator/communicator.h"
#include "ompi/mpi/c/bindings.h"
#include "ompi/errhandler/errhandler.h"
#include "ompi/constants.h"

static const char FUNC_NAME[] = "MPI_T_catagory_get_num";

int MPI_T_category_get_num (int *num_cat)
{
    mpit_lock ();

    if (0 == mpit_init_count) {
        mpit_unlock ();
        return MPI_T_ERR_NOT_INITIALIZED;
    }

    if (MPI_PARAM_CHECK) {
        if (NULL == num_cat) {
            mpit_unlock();
            return MPI_ERR_ARG;
        }
    }

    *num_cat = mca_base_param_group_get_count ();

    mpit_unlock ();

    return MPI_SUCCESS;
}
