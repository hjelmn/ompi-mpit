/* -*- Mode: C; c-basic-offset:4 ; indent-tabs-mode:nil -*- */
/*
 * Copyright (c) 2012      Los Alamos National Security, LLC. All rights
 *                         reserved.
 * $COPYRIGHT$
 *
 * Additional copyrights may follow
 *
 * $HEADER$
 */

#include "ompi/include/ompi_config.h"

#include "opal/mca/base/mca_base_param.h"

#include "ompi/runtime/params.h"
#include "ompi/communicator/communicator.h"
#include "ompi/mpi/c/bindings.h"
#include "ompi/errhandler/errhandler.h"
#include "ompi/constants.h"

#include "ompi/mpit/mpit-internal.h"

static const char FUNC_NAME[] = "MPI_T_category_get_info";

int MPI_T_category_get_info(int cat_index, char *name, int *name_len,
                            char *desc, int *desc_len, int *num_cvars,
                            int *num_pvars, int *num_categories)
{
    mca_base_param_group_info_t group_info;
    int rc;

    mpit_lock ();

    if (0 == mpit_init_count) {
        mpit_unlock ();
        return MPI_T_ERR_NOT_INITIALIZED;
    }

    rc = mca_base_param_group_dump (cat_index, &group_info);
    if (0 > rc) {
        mpit_unlock ();
        if (OPAL_ERR_NOT_FOUND == rc) {
            return MPI_T_ERR_INVALID_INDEX;
        }

        return rc;
    }

    /* XXX -- TODO -- Implement me */
    if (NULL != num_pvars) {
        *num_pvars = 0;
    }

    if (NULL != num_cvars) {
        *num_cvars = group_info.mbpg_param_count;
    }

    if (NULL != num_categories) {
        *num_categories = group_info.mbpg_subgroup_count;
    }

    mpit_copy_string (name, name_len, group_info.mbpg_name);
    mpit_copy_string (desc, desc_len, group_info.mbpg_help_msg);

    OBJ_DESTRUCT(&group_info);

    mpit_unlock ();

    return MPI_SUCCESS;
}
