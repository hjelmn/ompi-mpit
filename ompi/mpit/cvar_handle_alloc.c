/* -*- Mode: C; c-basic-offset:4 ; indent-tabs-mode:nil -*- */
/*
 * Copyright (c) 2012      Los Alamos National Security, LLC. All rights
 *                         reserved.
 * $COPYRIGHT$
 *
 * Additional copyrights may follow
 *
 * $HEADER$
 */


#include "ompi/include/ompi_config.h"

#include "opal/mca/base/mca_base_param.h"

#include "ompi/runtime/params.h"
#include "ompi/mpi/c/bindings.h"
#include "ompi/errhandler/errhandler.h"
#include "ompi/constants.h"

#include "ompi/mpit/mpit-internal.h"

static const char FUNC_NAME[] = "MPI_T_cvar_handle_alloc";

int MPI_T_cvar_handle_alloc (int cvar_index, void *obj_handle,
                             MPI_T_cvar_handle *handle, int *count)
{
    ompi_mpit_cvar_handle_t *new_handle;
    int rc;

    mpit_lock ();

    *handle = NULL;

    if (0 == mpit_init_count) {
        mpit_unlock ();
        return MPI_T_ERR_NOT_INITIALIZED;
    }

    /* Validate the index */
    if (cvar_index < 0 || cvar_index >= mca_base_param_get_count ()) {
        mpit_unlock ();
        return MPI_T_ERR_INVALID_INDEX;
    }

    if (MPI_PARAM_CHECK) {
        if (NULL == handle || NULL == count) {
            mpit_unlock();
            return MPI_ERR_ARG;
        }
    }

    new_handle = (ompi_mpit_cvar_handle_t *) malloc (sizeof (ompi_mpit_cvar_handle_t));
    if (NULL == new_handle) {
        return MPI_T_ERR_MEMORY;
    }

    rc = mca_base_param_dump_index (cvar_index, &new_handle->info);
    if (OPAL_SUCCESS != rc) {
        free (new_handle);
        return MPI_ERR_OTHER;
    }

    new_handle->bound_object = obj_handle;

    if (MCA_BASE_PARAM_TYPE_INT == new_handle->info->mbpp_type) {
        /* MCA only supports a single integer at this time. Change me if
           this assumption changes. */
        *count = 1;
    } else if (MCA_BASE_PARAM_TYPE_STRING == new_handle->info->mbpp_type) {
        /* Arbitrary string limit. Is there a better way to do this? */
        *count = 2048;
    }

    *handle = (MPI_T_cvar_handle) new_handle;
    
    mpit_unlock ();

    return MPI_SUCCESS;
}
