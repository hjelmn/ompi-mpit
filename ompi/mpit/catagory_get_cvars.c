/* -*- Mode: C; c-basic-offset:4 ; indent-tabs-mode:nil -*- */
/*
 * Copyright (c) 2012      Los Alamos National Security, LLC. All rights
 *                         reserved.
 * $COPYRIGHT$
 *
 * Additional copyrights may follow
 *
 * $HEADER$
 */

#include "ompi/include/ompi_config.h"

#include "opal/mca/base/mca_base_param.h"

#include "ompi/runtime/params.h"
#include "ompi/communicator/communicator.h"
#include "ompi/mpi/c/bindings.h"
#include "ompi/errhandler/errhandler.h"
#include "ompi/constants.h"

#include "ompi/mpit/mpit-internal.h"

static const char FUNC_NAME[] = "MPI_T_category_get_cvars";

int MPI_T_category_get_cvars(int cat_index, int len, int indices[])
{
    mca_base_param_group_info_t group_info;
    int rc, i;

    mpit_lock ();

    if (!mpit_is_initialized ()) {
        mpit_unlock ();
        return MPI_T_ERR_NOT_INITIALIZED;
    }

    rc = mca_base_param_group_dump (cat_index, &group_info);
    if (0 > rc) {
        mpit_unlock ();
        if (OPAL_ERR_NOT_FOUND == rc) {
            return MPI_T_ERR_INVALID_INDEX;
        }

        return rc;
    }

    for (i = 0 ; i < len && i < group_info.mbpg_param_count ; ++i) {
        indices[i] = group_info.mbpg_params[i];
    }

    OBJ_DESTRUCT(&group_info);

    mpit_unlock ();

    return MPI_SUCCESS;
}
